# Python & Ferris Resources

## Learning Python

* [Python Practice Book](http://anandology.com/python-practice-book/index.html)
* [Python in 4 Hours](http://life.bsc.es/pid/brian/python/#/)

## Python Libraries

### Time and Dates

* [dmc: Python Dates and Times, the right way.](https://github.com/rhettg/dmc)
* [Arrow: better dates and times for Python](http://crsmithdev.com/arrow/)

### Text Extraction

* [textract: Extract text from any document; no muss, no fuss.](http://textract.readthedocs.org/en/latest/index.html)

### Text Formatting

* [python-ftfy: Given Unicode text, make its representation consistent and possibly less broken.](https://github.com/LuminosoInsight/python-ftfy)

## Ferris Framework

### Index page

    @route_with('/')
    def index(self):
        pass

### Restrict HTTP methods on your controller actions

    @route_with('/login', ['POST'])
    def login_post(self):
        pass

* * *

compiled by [Ben Sarmiento](mailto:ben.sarmiento@cloudsherpas.com)
